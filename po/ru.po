# Russian translation for upnp-router-control
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the upnp-router-control package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: upnp-router-control\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-09 14:30+0200\n"
"PO-Revision-Date: 2021-07-09 23:42+0000\n"
"Last-Translator: Alexander 'FONTER' Zinin <spore_09@mail.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2023-05-09 12:36+0000\n"
"X-Generator: Launchpad (build 90e2150bef31b411d7bae5c4032c7e320fcaaec8)\n"
"Language: ru\n"

#: src/urc-main.c:56 data/upnp-router-control.desktop.in:4
#: data/org.upnproutercontrol.UPnPRouterControl.appdata.xml.in:5
msgid "UPnP Router Control"
msgstr "UPnP Router Control"

#: src/urc-gui.c:147
msgid "Unable to set this port forward"
msgstr "Невозможно перенаправить этот порт"

#: src/urc-gui.c:333
msgid "Unable to remove this port forward"
msgstr "Невозможно отменить перенаправление этого порта"

#: src/urc-gui.c:373
msgid "Description"
msgstr "Описание"

#: src/urc-gui.c:374
msgid "Protocol"
msgstr "Протокол"

#: src/urc-gui.c:375
msgid "Int. Port"
msgstr "Внутр. порт"

#: src/urc-gui.c:376
msgid "Ext. Port"
msgstr "Внеш. порт"

#: src/urc-gui.c:377
msgid "Local IP"
msgstr "Локальный IP"

#: src/urc-gui.c:575 src/urc-gui.c:577 src/urc-gui.c:579 src/urc-gui.c:581
#: src/urc-gui.c:583 src/urc-gui.c:600 src/urc-gui.c:743
msgid "WAN status:"
msgstr "Статус WAN:"

#: src/urc-gui.c:575
msgid "Connected"
msgstr "Подключено"

#: src/urc-gui.c:577
msgid "Disconnected"
msgstr "Отключено"

#: src/urc-gui.c:579
msgid "Connecting"
msgstr "Подключение"

#: src/urc-gui.c:581
msgid "Disconnecting"
msgstr "Отключение"

#: src/urc-gui.c:600 src/urc-gui.c:638
msgid "unknown"
msgstr "неизвестно"

#: src/urc-gui.c:618 src/urc-gui.c:620 src/urc-gui.c:638 src/urc-gui.c:748
msgid "IP:"
msgstr "IP:"

#: src/urc-gui.c:618
msgid "none"
msgstr "нет"

#: src/urc-gui.c:657
#, c-format
msgid "Connected to %s"
msgstr ""

#: src/urc-gui.c:691
msgid "Open the router config in the default browser"
msgstr "Открыть страницу настройки маршрутизатора в браузере по умолчанию"

#: src/urc-gui.c:695 src/urc-gui.c:740 src/urc-gui.c:743 src/urc-gui.c:748
#: src/urc-gui.c:759
msgid "not available"
msgstr "недоступно"

#: src/urc-gui.c:772
msgid "Devices discovery started…"
msgstr ""

#. Feel free to put your names here translators :-)
#: src/urc-gui.c:822
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Alexander 'FONTER' Zinin https://launchpad.net/~spore-09\n"
"  Daniele Napolitano https://launchpad.net/~dnax88\n"
"  Oleg \"Nightwing\" Lomakin https://launchpad.net/~nightwing666"

#: src/urc-gui.c:828 data/upnp-router-control.desktop.in:5
#: data/org.upnproutercontrol.UPnPRouterControl.appdata.xml.in:8
msgid "A simple program that manages UPnP compliant routers"
msgstr ""

#: src/urc-graph.c:171
#, c-format
msgid "%u seconds"
msgstr "%u секунд"

#: src/urc-main-window.ui:50
msgid "Config:"
msgstr "Настройки:"

#: src/urc-main-window.ui:94
msgid "Name:"
msgstr ""

#: src/urc-main-window.ui:205
msgid "_Add"
msgstr ""

#: src/urc-main-window.ui:219
msgid "_Remove"
msgstr ""

#: src/urc-main-window.ui:251
msgid "Network history"
msgstr "История сети"

#: src/urc-main-window.ui:315
msgid "Receiving"
msgstr ""

#: src/urc-main-window.ui:331
msgid "Total Received"
msgstr ""

#: src/urc-main-window.ui:399
msgid "Sending"
msgstr ""

#: src/urc-main-window.ui:415
msgid "Total Sent"
msgstr ""

#: src/urc-main-window.ui:469
msgid "Note: The network speed calculation it's approximate"
msgstr ""

#: src/urc-main-window.ui:494
msgid "Refresh"
msgstr ""

#: src/upnp-router-control-headermenu.ui:7
msgid "Router _info"
msgstr ""

#: src/upnp-router-control-headermenu.ui:13
msgid "_About"
msgstr ""

#: src/urc-add-port-window.ui:16
msgid "Add new port forward"
msgstr "Добавить новое перенаправление порта"

#: src/urc-add-port-window.ui:26
msgid "_Cancel"
msgstr ""

#: src/urc-add-port-window.ui:35
msgid "_Apply"
msgstr ""

#: src/urc-add-port-window.ui:72
msgid "Description:"
msgstr "Описание:"

#: src/urc-add-port-window.ui:98
msgid "Insert here a short description for this port"
msgstr "Вставте сюда краткое описание этого порта"

#: src/urc-add-port-window.ui:113
msgid "External port:"
msgstr "Внеший порт:"

#: src/urc-add-port-window.ui:127
msgid "Insert here the port exposed by the router to Internet"
msgstr "Вставьте сюда порт открываемый маршрутизатором в Интернет"

#: src/urc-add-port-window.ui:143
msgid "Protocol:"
msgstr "Протокол:"

#: src/urc-add-port-window.ui:157
msgid "Select here the protocol used, usually TCP"
msgstr "Выберете используемый протокол, обычно это TCP"

#: src/urc-add-port-window.ui:269
msgid "Local IP:"
msgstr "Локальный IP:"

#: src/urc-add-port-window.ui:281
msgid "Local port:"
msgstr "Локальный порт:"

#: src/urc-add-port-window.ui:296
msgid ""
"Insert here the local IP where forward packets, usually IP of this computer"
msgstr ""
"Вставьте сюда локальный IP на который будут направляться пакеты, обычно это "
"IP этого компьютера"

#: src/urc-add-port-window.ui:315
msgid ""
"Insert the the local port where forward the packets from router to your "
"computer"
msgstr ""
"Встатьте сюда номер локального порта на который будут перенаправляться "
"пакеты с маршрутизатора на ваш компютер"

#: src/urc-add-port-window.ui:362
msgid "Advanced parameters"
msgstr "Дополнительные параметры"

#: src/urc-info-window.ui:55
msgid "Manufacturer"
msgstr ""

#: src/urc-info-window.ui:71
msgid "Manufacturer website"
msgstr ""

#: src/urc-info-window.ui:87
msgid "Model name"
msgstr ""

#: src/urc-info-window.ui:103
msgid "Model number"
msgstr ""

#: src/urc-info-window.ui:172
msgid "Model Description"
msgstr ""

#: src/urc-info-window.ui:202
msgid "UPC"
msgstr ""

#: src/urc-info-window.ui:231
msgid "Serial number"
msgstr ""

#: src/urc-info-window.ui:274
msgid "XML Descriptor"
msgstr ""

#: data/org.upnproutercontrol.UPnPRouterControl.appdata.xml.in:13
msgid ""
"A GTK application to access some parameters of the router like: the network "
"speed, the external IP and the model name. It can manage port forwarding "
"through a simple GUI interface."
msgstr ""

#: data/org.upnproutercontrol.UPnPRouterControl.appdata.xml.in:18
msgid ""
"UPnP Router Control uses the Internet Gateway Device Protocol v1, so UPnP it "
"must be enabled on your router in order to use its functions."
msgstr ""

#~ msgid "Enabled"
#~ msgstr "Включено"

#~ msgid "Brand"
#~ msgstr "Производитель"

#~ msgid "Model Name:"
#~ msgstr "Модель:"

#~ msgid "Model Number:"
#~ msgstr "Номер модели:"

#~ msgid "Brand website:"
#~ msgstr "Сайт производителя:"

#~ msgid "A simple program to manage UPnP IGD compliant routers"
#~ msgstr ""
#~ "Простая программа для управления UPnP IGD-совместимыми маршрутизаторами"

#~ msgid "Router name:"
#~ msgstr "Имя маршрутизатора:"

#~ msgid "Model Description:"
#~ msgstr "Описание модели:"
